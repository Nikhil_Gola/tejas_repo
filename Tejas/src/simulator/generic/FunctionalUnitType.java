package generic;

/* Functional unit definitions
 * */
public enum FunctionalUnitType {

	inValid,
	integerALU,
	integerMul,
	integerDiv,
	floatALU,
	floatMul,
	floatDiv,
	memory,
	no_of_types
	
}
