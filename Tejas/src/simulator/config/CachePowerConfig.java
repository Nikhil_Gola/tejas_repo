package config;

public class CachePowerConfig {
	public double leakagePower;
	public double readDynamicPower;
	public double writeDynamicPower;
}
