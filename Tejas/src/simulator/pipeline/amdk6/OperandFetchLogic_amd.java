package pipeline.amdk6;

import generic.Core;
import generic.Event;
import generic.EventQueue;
import generic.PortType;
import generic.SimulationElement;
/* Code for fetching operands for an instruction
 * */
public class OperandFetchLogic_amd extends SimulationElement {
	
	private Core core;
	private OutOrderExecutionEngine_amd containingExecutionEngine;
	private InstructionWindow_amd IW;
	private int issueWidth;
	private RenameLogic_amd renameModule;

	public OperandFetchLogic_amd(Core core, OutOrderExecutionEngine_amd execEngine)
	{
		super(PortType.Unlimited, -1, -1 ,core.getEventQueue(), -1, -1);
		this.core = core;
		this.containingExecutionEngine = execEngine;
		issueWidth = core.getIssueWidth();
		IW = execEngine.getInstructionWindow();
		renameModule = new RenameLogic_amd(core, execEngine);
	}
	
	public void performOperandFetch() {
		
		/* As mentioned in the design of the document as well as the AMD K-6 pipeline,
		 * renaming is done before operand fetching. we have called this after taking care if any stalls
		 * This is a design choice.
		 * */
		renameModule.performRename();
		
		/* If pipeline is stalled due to mis prediction
		 * The following loop can be removed but this gives a close simulation of the program. ASK?
		 * */
		if(containingExecutionEngine.isToStall5())
		{
			//pipeline stalled due to branch mis-prediction
			return;
		}
		
		for(int i = 0; i < IW.getMaxIWSize(); i++) {
			
			IWEntry_amd iwEntry = IW.getIW()[i];
			if(iwEntry != null && iwEntry.isValid && !iwEntry.oneCyclePassed) {
			
				/*TODO
				 * in issue stage check whether an FU is available. 
				 * Issue only those instructions for which FU is available..
				 */
				iwEntry.issueInstruction(); 
				//IW.removeFromWindow(iwEntry); // remove instruction windows from where?
			}
		}
	}

	@Override
	public void handleEvent(EventQueue eventQ, Event event) {
		// TODO Auto-generated method stub
		
	}
}
