package pipeline.amdk6;

import generic.Event;
import generic.RequestType;
import generic.SimulationElement;

public class BroadCastEvent_amd extends Event {
	
	ReorderBufferEntry_amd ROBEntry;

	public BroadCastEvent_amd(long eventTime,
			SimulationElement requestingElement,
			SimulationElement processingElement,
			RequestType requestType,
			ReorderBufferEntry_amd ROBEntry)
	{
		super(null, eventTime, requestingElement, processingElement, requestType, -1);
		
		this.ROBEntry = ROBEntry;
	}

	public ReorderBufferEntry_amd getROBEntry() {
		return ROBEntry;
	}

}
