package pipeline.amdk6;

import generic.*;
/*	Add constructor for amdk6*/

public class ExecCompleteEvent_amd extends Event {
	
	ReorderBufferEntry_amd ROBEntry;

	public ExecCompleteEvent_amd(EventQueue eventQ,
			long eventTime,
			SimulationElement requestingElement,
			SimulationElement processingElement,
			RequestType requestType,
			ReorderBufferEntry_amd ROBEntry)
	{
		super(eventQ, eventTime, requestingElement, processingElement, requestType, -1);
		
		this.ROBEntry = ROBEntry;
	}

	public ReorderBufferEntry_amd getROBEntry() {
		return ROBEntry;
	}

}
