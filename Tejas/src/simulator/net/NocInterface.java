/*****************************************************************************
				Tejas Simulator
------------------------------------------------------------------------------------------------------------

   Copyright [2010] [Indian Institute of Technology, Delhi]
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
------------------------------------------------------------------------------------------------------------

	Contributors:  Eldhose Peter, Rajshekar
*****************************************************************************/
package net;

import generic.Port;
import generic.SimulationElement;

import java.util.Vector;
/*****************************************************
 * 
 * NocInterface to make the router generic
 *
 *****************************************************/
public interface NocInterface{
	
	public Router getRouter();
	public Vector<Integer> getId();
	public Port getPort();
	public SimulationElement getSimulationElement();
	
}
