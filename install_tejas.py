import commands
import xml.etree.ElementTree as ET
import os, sys

pwd = commands.getoutput('pwd')
print "\npwd is " + pwd

#print dependencies
print "\nrequired :"
print "\t1) Mercurial"
print "\t2) Java (1.7 and above)"
print "\t3) ant"
print "\t4) ia32-libs-multiarch (only for 64 bit machines)"
#check dependencies




#clone tejas
#copy PIN
#copy junit.jar
if os.path.exists(pwd + '/Tejas'):
	print "Tejas source already cloned"
else:
	if sys.version_info < (3,0):
		username = raw_input("enter palasi username : ")
	else:
		username = input("enter palasi username : ")
	print '\ngetting source please wait'
	status, output = commands.getstatusoutput("hg clone ssh://" + str(username) + "@palasi.cse.iitd.ac.in//home/phd/rajshekar/HPC/Tejas")
	if status != 0:
		print "error cloning : " + str(os.WEXITSTATUS(status))
		print output
		print '\n'
		sys.exit(1)
	else:
		print "cloning successful"

#extract PIN
print "\nextracting PIN"
if os.path.exists(pwd + '/PIN'):
	print "PIN already extracted"
else:
	status, output = commands.getstatusoutput("tar -xvf Tejas/PIN.tar.gz")
	if status != 0:
		print "error extracting PIN : " + str(os.WEXITSTATUS(status))
		print output
		sys.exit(1)



print '\nconfiguring'


#update build.xml
antfile = 'Tejas/build.xml'
print 'setting junit-path in ' + antfile + " to " + pwd + "/Tejas/junit.jar"
tree = ET.parse(antfile)
root = tree.getroot()
for prop in root.findall('property'):
	if prop.get('name') == 'junit-path':
		prop.set('value', pwd + '/Tejas/junit.jar')
tree.write(antfile, encoding="UTF-8", xml_declaration=True)

#update makefile.gnu.config
fname = 'Tejas/src/emulator/pin/makefile.gnu.config'
print 'setting PIN_KIT in ' + fname + " to " + pwd + "/PIN"
f = open(fname, 'r')
lines = f.readlines()
i = 0
for line in lines:
	if "PIN_KIT ?=" in line:
		lines[i] = "PIN_KIT ?= " + pwd + "/PIN" + "\n"
		break
	i = i + 1
f.close()
f = open(fname, 'w')
for line in lines:
	f.write(line)
f.close()

#update config.xml
fname  = 'Tejas/src/simulator/config/config.xml'
tree = ET.parse(fname)
root = tree.getroot()
emulator = root.find('Emulator')
print 'setting PinTool in ' + fname + ' to ' + pwd + '/PIN'
emulator.find('PinTool').text = pwd + "/PIN"
print 'setting PinInstrumentor in ' + fname + ' to ' + pwd + '/Tejas/src/emulator/pin/obj-pin/causalityTool.so'
emulator.find('PinInstrumentor').text = pwd + "/Tejas/src/emulator/pin/obj-pin/causalityTool.so"
print 'setting ShmLibDirectory in ' + fname + ' to ' + pwd + '/Tejas/src/emulator/pin/obj-comm'
emulator.find('ShmLibDirectory').text = pwd + "/Tejas/src/emulator/pin/obj-comm"
tree.write(fname, encoding="UTF-8", xml_declaration=True)

print "configure successful"



print '\nbuilding'
os.chdir('Tejas')
pwd = commands.getoutput('pwd')
print "pwd is " + pwd
status, output = commands.getstatusoutput("ant make-jar")
if status != 0 or os.path.exists(pwd + "/src/emulator/pin/obj-pin/causalityTool.so") == False or os.path.exists(pwd + "/src/emulator/pin/obj-comm/libshmlib.so") == False:
	print "error building : " + str(os.WEXITSTATUS(status))
	print output
	sys.exit(1)
else:
	print "build successful"


print '\ntest run - hello world'
os.chdir('..')
pwd=commands.getoutput('pwd')

if os.path.exists(pwd + '/outputs')==False:
	os.mkdir('outputs')
if os.path.exists(pwd + '/tests')==False:
	os.mkdir('tests')
	if os.path.exists(pwd + '/tests/hello_world.cpp')==False:
		f = open(pwd + '/tests/hello_world.cpp', 'w')
		f.write("#include<iostream>\nint main() {\nstd::cout<<\"hello world\"<<std::endl;\nreturn (0);\n}\n")
		f.close()	

cmd = "g++ " + pwd + "/tests/hello_world.cpp -o " + pwd + "/tests/hello_world.o"
print cmd
status, output = commands.getstatusoutput(cmd)
if status != 0 or os.path.exists(pwd + "/tests/hello_world.o") == False:
	print "error compiling test file : " + str(os.WEXITSTATUS(status))
	print output
	sys.exit(1)

cmd = "java -jar " + pwd + "/Tejas/jars/tejas.jar " + pwd + "/Tejas/src/simulator/config/config.xml " + pwd + "/outputs/hello_world.output " + pwd + "/tests/hello_world.o"
print cmd
status = os.system(cmd)


if status != 0 or os.path.exists(pwd + "/outputs/hello_world.output") == False:
	print "error running test : " + str(os.WEXITSTATUS(status))
	sys.exit(1)
else:
	print "test run successful. please see " + pwd + "/outputs/hello_world.output for the result of simulating the 'hello_world' program"

